require 'compass/import-once/activate'
# Require any additional compass plugins here.

add_import_path "www/lib/foundation/scss"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "assets/build/css"
sass_dir = "assets/src/sass"
images_dir = "assets/img"
javascripts_dir = "assets/build/js"
