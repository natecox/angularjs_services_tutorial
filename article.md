---
title: AngularJS Services: Traversing the Information Void
author: Nathan Cox
author_link: https://github.com/natecox
categories: [Web Development, AngularJS]
tags: [angular, service, factory]
---

# AngularJS Services: Traversing the Information Void

## Introduction
As an AngularJS developer one finds oneself on a constant quest for knowledge;
a journey that for the most part is enjoyable and satisfying. Angular in
general has blossomed into a wonderful technology that is simply delightful
to use on a daily basis; I find myself remarking to my colleagues multiple
times daily regarding the benefits and wonders that comprise Angular.

However, as anyone who has ever read a halfway decent book can tell you, no
path is ever walked entirely in the light. Even the best of technologies
comes with its dark side, and for Angular the shadow in the road comes in the
form of _services_. Don't misunderstand, Angular services themselves are
absolutely awesome; it's the documentation on the subject which brings the
darkness.

Before we go too far: if you're just starting to learn angular (or need a
refresher) now would be a great time to go back and read through
[my article on writing single page applications in Angular]
(http://xepler.com/blog/2014/08/05/deploying-your-first-spa-a-guiding-light-through-the-abyss-of-web-technologies).

### So What's The Problem?
According to [the AngularJS docs](https://docs.angularjs.org/guide/services)
services are:
> ... substitutable objects that are wired together using dependency injection
> (DI). You can use services to organize and share code across your app.

_**For me, this was the point where things started to go horribly wrong.**_

You see, that's about all you're going to get on the subject from the official
documentation. Normally I would handle this by digging into the code and
finding context to give the description meaning, which is not an unreasonable
assumption someone writing the docs would make. However, the rest of the page
doesn't use a single `.service` call; instead it uses `.factory`.

So what's a factory? Good luck finding that one out; the docs don't reference
them at all that I've been able to find. When you look them up you get directed
to the services page. It's taken me the last several months of using Angular to
get a good handle on what a service and a factory actually are, when to use
them and how.

### Get To The Point, Already!
What follows is a general use case for AngularJS services and factories, which
(despite what you may read) can certainly be treated as different things. Keep
in mind that this is an _opinionated_ guide to using Angular, and many of the
ideas presented to you may be different than how you would prefer to use them.


### Can I see it in action?
All of the source code for this article is available in a
[github repo](https://github.com/natecox/angularjs_services_tutorial).

Also, a working example of the final code can be viewed
[here](http://natecox.github.io/angularjs_services_tutorial/).

## Services, Angular's Answer to Static Classes
First off we'll tackle the namesake of the article: services. A service is,
in essence, a means of creating a static class that angular can reference
from any point in the runtime. Once you have instantiated a service (which is
done by injecting it into any other angular element) you will have a globally
available reference to the _exact same instance_ from any other place. The key
here is that angular will only ever instantiate the service one time, and you
should not be trying to do so yourself with `new`.

### Defining A New Service
Service definitions themselves are fairly simple. We'll start by setting up
a simple module in `app.coffee`, and include a couple of states so that we
can navigate between them and watch how changes to the data model are updated:

    angular
    .module 'services_tutorial', ['ui.router']
    .config ($stateProvider, $urlRouterProvider)->
        $stateProvider
        .state 'main',
            url: ''
            abstract: true
            templateUrl: 'modules/base/templates/container.html'
        .state 'main.state_one',
            url: '/state_one'
            views:
                content:
                    templateUrl: 'modules/base/templates/state_one.html'
                    controller: 'FirstCtrl as first'
        .state 'main.state_two',
               url: '/state_two'
               views:
                   content:
                       templateUrl: 'modules/base/templates/state_two.html'
                       controller: 'SecondCtrl as second'
        .state 'about',
           url: '/about'
           templateUrl: 'modules/base/templates/about.html'

        $urlRouterProvider.otherwise('/state_one')

Again, if this is confusing to you please go back and read through
[my article on writing single page applications in Angular]
(http://xepler.com/blog/2014/08/05/deploying-your-first-spa-a-guiding-light-through-the-abyss-of-web-technologies).

Now that we've defined the angular module itself, we can move on to writing our
first service. The beautiful thing about services is that they can be as simple
or as complicated as you want them to be. Here's our `services.coffee`:

    MainService = ->
        @staticVar = 'Initial Value!'

        return

    angular
    .module 'services_tutorial'
    .service 'MainService', MainService

In case you weren't aware, `@` is a shortcut for `this.` in CoffeeScript.
What we've done here is define a new service called `MainService` which
contains a single variable `staticVar`. The return on the end is critical, by
the way. Without it coffee will return the variable instead of the object
itself and cause you all sorts of headaches debugging.

### Service Injection Into Controllers
Lets go ahead and put this new service to use. We're going to define two
controllers, each of which will reference `MainService`.
In `controllers.coffee`:

    FirstCtrl = (MainService) ->
        @service = MainService
        return

    SecondCtrl = (MainService) ->
        @service = MainService
        return

    angular
    .module 'services_tutorial'
    .controller 'FirstCtrl', FirstCtrl
    .controller 'SecondCtrl', SecondCtrl

Notice that we've assigned `MainService` to a scope variable in each
controller. If we didn't want two-way data binding with the service we could
just as easily use `@someVar = MainService.staticVar`, but for the purposes
of demonstration we're going to allow the controller to modify the service
variable directly. If you're going for encapsulation, you can write getters and
setters instead.

The important part of what just happened is that both controllers are
referencing the exact same instance of `MainService`. It was instantiated
when it was injected into `FirstCtrl`, and the injection into `SecondCtrl`
simply returned a reference.

### Using The Service In A Template
Lets go ahead and finish this skeleton off by including the templates we
assigned in `app.coffee`.

Here's `container.html`:

    <div class="row">
        <div class="small-3 columns">
            <ul class="side-nav">
                <li ui-sref-active="active">
                    <a ui-sref="main.state_one">State One</a>
                </li>
                <li ui-sref-active="active">
                    <a ui-sref="main.state_two">State Two</a>
                </li>
            </ul>
        </div>
        <div class="small-9 columns" ui-view="content"></div>
    </div>

This is `state_one.html`:

    FirstCtrl Service Variable: {{ first.service.staticVar }}
    <input type="text" ng-model="first.service.staticVar" />

This is `state_two.html`:

    SecondCtrl Service Variable: {{ second.service.staticVar }}
    <input type="text" ng-model="second.service.staticVar" />

Finally, we can just keep `about.html` blank for right now. It doesn't really
play a role in this tutorial, but has a strong sense of shameless
self-promotion in the finished product.

The real magic is happening in `state_one.html` and `state_two.html`. We've
pushed staticVar to the template in two ways. First as a text representation,
and second as a `ng-model` to a text input. Because of the `ng-model` directive
if we change the input value it will also change the value of `staticVar` in
`MainService`. This two-way binding is exactly what we want, because we can see
that if we modify the text on either page, the other one updates with it
seamlessly.

### Why Is This So Cool?
Now that we've seen the service in action, the question you should be asking
yourself now is, "Where and why would I actually use that?"

Services allow you to abstract common logic away from your controller, which is
a very good thing. In small applications there is a strong tendency to simply
put _all_ of your logic into the controller, and this is a very bad habit to
get into. One of the most awesome aspects of Angular is that we can implement
reliable structure that makes sense and keeps logic compartmentalized properly.

Controllers should be used for presentational logic only; it takes data that
has been processed and formatted elsewhere and determines how to get it onto
the template properly, defines functions to be run on user input and is
generally your gateway to user experience. Everything else that you do should
be moved away from them at all costs. When I first heard this I scoffed (just
like everyone else) but the fact is that once you start working on large,
extremely complicated applications your app will depend on your ability to
structure your app properly.

Retrieving data from a server, storing data that is used across the app,
parsing, formatting, interpolating, everything that revolves around _the data
itself_ should be done in a service or (as we'll see later) in a factory.
Trust me, six months into your project you will be singing the praises of
proper logic abstraction.

### In Summary...
Services are a great way to store away logic and data that needs to be accessed
by controllers, directives or filters. They are your friend in the world of
data modeling, so get to know them.


## Factories, Angular's Answer To Non-Static Classes
Now that we've gotten acquainted with services and see how useful they can
be for storing persistent information across an app, lets take a look at
factories. Much like services, information on using factories properly is
virtually non-existent. You will see factories used frequently in the
documentation, but they are _always_ referred to as services.

The fact is that factories and services are virtually the same thing as far
as angular is concerned. They share much of the same code, play a very similar
role and are injected in much the same way. The key difference between a
service and a factory is that when a _service_ is injected Angular
automatically instantiates it for you and returns the usable instance, whereas
factories are left for you to instantiate yourself.

Remember that in the above service examples all we had to do was inject
a service into _any_ controller (or directive, et cetera) to get a global
instance immediately. A factory _will not do this_, and injecting it into
multiple controllers will potentially net you multiple different instances
of that factory. That being said, once a factory is manually instantiated it
does become globally accessible; a fact that we are going to exploit.

The name 'factory' is actually very apt, it is a single bit of code that can be
used to create copies of itself. If you're familiar with object oriented
programming this is a very familiar concept; factories are really just
classes, but classes that Angular has inherent and special knowledge of.

In this next section we are going to learn how to define and instantiate a new
factory in a class to see multiple instances in action.

### Defining A New Factory
As with services, factories can be as complicated or as simple as you want them
to be. Lets start with a simple example.

In `services.coffee`:

    MainService = ->
        @staticVar = 'Initial Value!'

        return

    MainFactory = ->
        MainFactory = (input='default') ->
            @factoryVar = input

        MainFactory::get = -> @factoryVar

        MainFactory::set = (input) -> @factoryVar = input

        return MainFactory

    angular
    .module 'services_tutorial'
    .service 'MainService', MainService
    .factory 'MainFactory', MainFactory

Lets break this down: Inside the `MainFactory` function, we've defined a
`MainFactory` function. Yeah, this seems confusing at first, but the naming
here is intentional. The second function is scoped within the first one, so
nothing outside of that first function knows that the second one exists.
The naming is kept consistent just for clarity of what is happening.

The function declaration itself can be considered a constructor, and takes
an input argument. When you instantiate with the new keyword that function is
what will be initially run. We use CoffeeScript's `::` prototype shortcut here
to define two functions for the factory instance: get and set.

The flow here is that you will call `MainFactory` via new:
`somevar = new MainFactory('someInput')` and it will return to you an object
with prototyped functions. We can see this in action by adding the factory to
our controllers.

In `controllers.coffee`:

    FirstCtrl = (MainService, MainFactory) ->
        @service = MainService
        @instances = []
        @instances.push new MainFactory('factory one')
        @instances.push new MainFactory('factory two')

        return

    SecondCtrl = (MainService, MainFactory) ->
        @service = MainService
        @instances = []
        @instances.push new MainFactory('factory three')
        @instances.push new MainFactory('factory four')

        return

    angular
    .module 'services_tutorial'
    .controller 'FirstCtrl', FirstCtrl
    .controller 'SecondCtrl', SecondCtrl

All we've done here is add an array to each controller that contains two
instances of `MainFactory` each. We can now reach the instances on our template
as so:

In `state_one.html`:

    FirstCtrl Service Variable: {{ first.service.staticVar }}
    <input type="text" ng-model="first.service.staticVar" />
    <ul>
        <li ng-repeat="instance in first.instances">
            var: {{ instance.get() }}
        </li>
    </ul>

In `state_two.html`:

    SecondCtrl Service Variable: {{ second.service.staticVar }}
    <input type="text" ng-model="second.service.staticVar" />
    <ul>
        <li ng-repeat="instance in second.instances">
            var: {{ instance.get() }}
        </li>
    </ul>

As you can see, nothing complicated is happening here. We're iterating through
our list of instances and for each one we're calling that instance's `get`
method, which returns the variable we passed in to the constructor.

### So That's Cool, But...
The problem with this method is twofold:

1. Our controllers are including non-presentational logic by manually
instantiating factories, and
2. Our factories aren't visible outside of the controllers, because there is
no way to reference them directly.

In the final segment of this article we're going to explore how to use
services and factories together to create an easy way to track multiple
instances of a factory.

## A Proper Use Pattern
Now that you know what services and factories actually are, it's time to
put them together to make something awesome. What we're going to do is create
a service which contains a list of factory instances, and a method of getting
a specific instance. Lets start with the Service itself.

    MainService = (MainFactory) ->
        @staticVar = 'Initial Value!'
        @factories = []

        # Adds a new factory to the service
        @add = (input) -> @factories.push new MainFactory input

        # Returns a list of matching factories
        @get = (input) ->
            temp = []
            for f in @factories
                temp.push f if f.get() is input

            return temp

        return

    MainFactory = ->
        MainFactory = (input='default') ->
            @factoryVar = input

        MainFactory::get = -> @factoryVar

        MainFactory::set = (input) -> @factoryVar = input

        return MainFactory

    angular
    .module 'services_tutorial'
    .service 'MainService', MainService
    .factory 'MainFactory', MainFactory

We've added four things to the service:

1. MainFactory has been injected into the service.
1. An array to hold factories
1. A method for adding factories
1. A method for returning a list of matching factories.

Pretty simple, right? Lets put it to use.

In `controllers.coffee`:

    FirstCtrl = (MainService) ->
        @service = MainService

        MainService.add 'Controller one'
        @instances = MainService.get('Controller one')

        return

    SecondCtrl = (MainService) ->
        @service = MainService

        MainService.add 'Controller two'
        @instances = MainService.get('Controller two')

        return

    angular
    .module 'services_tutorial'
    .controller 'FirstCtrl', FirstCtrl
    .controller 'SecondCtrl', SecondCtrl

Again, simple changes.

1. MainFactory is no longer being injected into the controllers, as there is
no need to talk to it directly anymore.
1. A call to `MainService.add` is being made to add a new factory instance.
1. A call to `MainService.get` is being made to return all matching factory
instances.

The templates themselves don't need to be updated at all this time, because
the structure of the controller is the same.

### So What's Really Happening Here?
The result of our (now finalized) code is fairly simple. We have:

* A service, which is a persistent element of the app and stores data for
access by all other elements.
* A factory, which is used to create unique data elements that can be accessed
from other elements.
* Controllers, which talk to services and to templates and are responsible
for the data on specific templates.

We're using our service to store many instances of our factory, and is
responsible for returning those instances to controllers for use in templates.
In a fully implemented app, the service would also be responsible for asking
the factories to update themselves, and possibly for handing the data to the
factory for that purpose.

If we were getting our actual data from a server somewhere via an API call
(i.e., from $http or a $resource) the proper flow would be to have the factory
update itself from the API, and simply have the service be responsible for
telling the factories _when_ to update (if that's even necessary).

An interesting thing to note here is that every time you navigate to a state,
it reloads the associated controller. This means that because the controller
is adding an element to the service, every time you navigate there it will
create a new entry which will be visible. Try clicking back and forth a few
times and you'll see what I mean; the list will keep getting longer.  This is
important to know, because it's a real gotcha situation if you're not
aware of it.

## ... And Knowing Is Half The Battle!
We've obviously covered a lot of material here for what seem like such simple
concepts. If you take only one thing away from this article, make it this:
A service is used when you only want one instance, period; A factory is used
when you want lots of copies made. It really is that simple.

As a friendly reminder, keep in mind that the examples provided here are only
the bare minimum required to properly demonstrate the difference between these
two Angular elements, and that much of what has been presented is highly
_opinionated_. If you go out and do more research on the subject you're going
to find many opinions of how services and factories should be implemented,
many of them are going to contradict each other and it is left to you to decide
what the _correct_ usage pattern is for yourself.

Hopefully I've been able to shed some light on the subject, and with luck
you'll be able to turn around and use what you've learned to make your life
much easier.
