MainService = (MainFactory) ->
    @staticVar = 'Initial Value!'
    @factories = []

    # Adds a new factory to the service
    @add = (input) -> @factories.push new MainFactory input

    # Returns a list of matching factories
    @get = (input) ->
        temp = []
        for f in @factories
            temp.push f if f.get() is input

        return temp

    return

MainFactory = ->
    MainFactory = (input='default') ->
        @factoryVar = input

    MainFactory::get = -> @factoryVar

    MainFactory::set = (input) -> @factoryVar = input

    return MainFactory

angular
.module 'services_tutorial'
.service 'MainService', MainService
.factory 'MainFactory', MainFactory
