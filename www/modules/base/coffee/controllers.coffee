FirstCtrl = (MainService) ->
    @service = MainService

    MainService.add 'Controller one'
    @instances = MainService.get('Controller one')

    return

SecondCtrl = (MainService) ->
    @service = MainService

    MainService.add 'Controller two'
    @instances = MainService.get('Controller two')

    return

angular
.module 'services_tutorial'
.controller 'FirstCtrl', FirstCtrl
.controller 'SecondCtrl', SecondCtrl
