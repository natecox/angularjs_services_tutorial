angular
.module 'services_tutorial', [
    'ngAnimate'
    'ui.router'
]
.config ($stateProvider, $urlRouterProvider)->
    $stateProvider
    .state 'main',
        url: ''
        abstract: true
        templateUrl: 'modules/base/templates/container.html'

    .state 'main.state_one',
        url: '/state_one'
        views:
            content:
                templateUrl: 'modules/base/templates/state_one.html'
                controller: 'FirstCtrl as first'

    .state 'main.state_two',
           url: '/state_two'
           views:
               content:
                   templateUrl: 'modules/base/templates/state_two.html'
                   controller: 'SecondCtrl as second'

    .state 'about',
       url: '/about'
       templateUrl: 'modules/base/templates/about.html'

    $urlRouterProvider.otherwise('/state_one')
